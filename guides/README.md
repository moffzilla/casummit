
# Activation Instructions 

 
   1. Once the customer has received the CAS Trial Activation Email (see image below), they will need to click through each of the hyperlinks to activate the corresponding product (Cloud Assembly, Service Broker, Code Stream)
   2. Once the customer clicks the hyperlink, they will be taken to console.cloud.vmware.com and will be prompted to login using their My VMware ID.
   3. After login, the customer will then be prompted to select a ‘fund’ for purposes of the trial. He/she should have a complimentary SPP fund of $100 that they need to select as a billing method.
   4. Choose the complimentary fund and click ‘Select Fund and Continue’ for next steps.
   5. If the fund is not showing, please advise NOT to continue using their Credit Card info as they will be billed automatically after the 30 day mark. 
   6. After fund selection, the next page will ask the customer to create a new Org. or link to an existing one. Customer should create a new Org. for purposes of the trial. This will ensure that the trial org. is not associated to other existing orgs and billing.
   7. Once the org is created, the customer will be taken to the Cloud Automation Services portal and confirm that the product is activated.
   8. Customer can validate product is activated by checking console.cloud.vmware.com OR clicking on the top right grid of squares.
   9. Customer will need to repeat the steps for the remaining products.

# Add additional team members 

   Once the customer has activated all the products, they can add additional team members using these instructions –
How to invite others to CAS (once you’ve activated the services):
 
   1. Login to your VMware Cloud Console (console.cloud.vmware.com) and select Cloud Assembly.
   2. On the top right of your Cloud Assembly console there will be a grid of squares that you can click (next to your org. name).
   3. Click the grid for a drop down list and click Identity and Access Management
   4. Once in IAM, you can assign roles and invite others to each of the CAS services. I recommend adding users as Admins or Owners for purposes of the POC/trial.
   5. Invitees will receive an email that will allow them access to the org

# Full list of technical prerequisites  

   https://docs.vmware.com/en/VMware-Cloud-Assembly/services/Getting-Started/GUID-D319E5A5-558E-4E5D-A73E-C3A9412254A5.html

   



name: Wordpress-Advanced
version: v1
iteration: 1
description: More Complex BP with External Storage
inputs:
  env:
    type: string
    enum:
      - 'env:azure'
      - 'env:vcenter'
    default: 'env:azure'
    title: Environment
    description: Target Environment
  location:
    type: string
    enum:
      - 'location:southcentralus'
      - 'location:dfw'
    default: 'location:southcentralus'
    title: Location
    description: Location
  size:
    type: string
    enum:
      - POC_Lab-Medium
      - POC_Lab-Small
    description: Size of Nodes
    title: Database Tier Size
  username:
    type: string
    minLength: 4
    maxLength: 20
    pattern: '[a-z]+'
    title: Database Username
    description: Database Username
  userpassword:
    type: string
    pattern: '[a-z0-9A-Z@#$]+'
    encrypted: true
    title: Database Password
    description: Database Password
  databaseDiskSize:
    type: number
    default: 4
    maximum: 10
    title: MySQL Data Disk Size
    description: Size of database disk
  count:
    type: integer
    default: 2
    maximum: 5
    minimum: 2
    title: Wordpress Cluster Size
    description: Wordpress Cluster Size (Number of nodes)
  archiveDiskSize:
    type: number
    default: 4
    maximum: 10
    title: Wordpress Archive Disk Size
    description: Size of Wordpress archive disk
resources:
  DBTier:
    type: Cloud.Machine
    properties:
      name: mysql
      image: Linux-POC_Lab
      tags:
        - key: Application
          value: MySQL
        - key: Environment
          value: QA
      flavor: '${input.size}'
      constraints:
        - tag: '${input.env}'
        - tag: '${input.location}'
      networks:
        - name: '${WP-Network.name}'
      remoteAccess:
        authentication: usernamePassword
        username: '${input.username}'
        password: '${input.userpassword}'
      cloudConfig: |
        #cloud-config
        repo_update: true
        repo_upgrade: all

        packages:
         - mysql-server

        runcmd:
         - sed -e '/bind-address/ s/^#*/#/' -i /etc/mysql/mysql.conf.d/mysqld.cnf
         - service mysql restart
         - mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'mysqlpassword';"
         - mysql -e "FLUSH PRIVILEGES;"
      attachedDisks: []
  WebTier:
    type: Cloud.Machine
    properties:
      name: wordpress
      image: Linux-POC_Lab
      tags:
        - key: Application
          value: Web Server
        - key: Environment
          value: QA
      count: 3
      constraints:
        - tag: '${input.env}'
        - tag: '${input.location}'
      flavor: POC_Lab-Small
      networks:
        - name: '${WP-Network.name}'
      cloudConfig: |
        #cloud-config
        repo_update: true
        repo_upgrade: all

        packages:
         - apache2
         - php
         - php-mysql
         - libapache2-mod-php
         - php-mcrypt
         - mysql-client

        runcmd:
         - mkdir -p /var/www/html/mywordpresssite && cd /var/www/html && wget https://wordpress.org/latest.tar.gz && tar -xzf /var/www/html/latest.tar.gz -C /var/www/html/mywordpresssite --strip-components 1
         - i=0; while [ $i -le 5 ]; do mysql --connect-timeout=3 -h ${DBTier.networks[0].address} -u root -pmysqlpassword -e "SHOW STATUS;" && break || sleep 15; i=$((i+1)); done
         - mysql -u root -pmysqlpassword -h ${DBTier.networks[0].address} -e "create database wordpress_blog;"
         - mv /var/www/html/mywordpresssite/wp-config-sample.php /var/www/html/mywordpresssite/wp-config.php
         - sed -i -e s/"define('DB_NAME', 'database_name_here');"/"define('DB_NAME', 'wordpress_blog');"/ /var/www/html/mywordpresssite/wp-config.php && sed -i -e s/"define('DB_USER', 'username_here');"/"define('DB_USER', 'root');"/ /var/www/html/mywordpresssite/wp-config.php && sed -i -e s/"define('DB_PASSWORD', 'password_here');"/"define('DB_PASSWORD', 'mysqlpassword');"/ /var/www/html/mywordpresssite/wp-config.php && sed -i -e s/"define('DB_HOST', 'localhost');"/"define('DB_HOST', '${DBTier.networks[0].address}');"/ /var/www/html/mywordpresssite/wp-config.php
         - service apache2 reload
  WP-Network:
    type: Cloud.Network
    properties:
      name: WP-Network
      networkType: existing
      constraints:
        - tag: '${input.env}'
        - tag: '${input.location}'
